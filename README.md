# HSVtoRGB(3) Programmer's Manual

NAME
----

HSVtoRGB - Library for converting HSV to RGB (Mainly created for arduino)

SYNOPSIS
--------

```c++
#include "HSVtoRGB.hpp"

RGB HSVtoRGB(float hue, float saturation, float value);
```

DESCRIPTION
-----------

The HSVtoRGB is a function that can convert a hue, saturation and value to red green and blue color values.
This can be used when you for example want to walk through the color circle smoothly (by changing the hue smoothly),
or if you want a specific hue with different saturation or brightness values.

 * Hue - An expected angle (in degrees) on the color circle
 * Saturation - The saturation of the color in a range of [0-1]
 * Value - The value (black and white mixture) of the color in a range of [0-1]

See NOTES for behaviours with values out of range

RETURN
------

Upon successful return, this function returns a RGB struct containing the red green and blue color values in a range of [0-1].
The struct is declared as follows:
```c++
struct RGB
{
    float r, g, b; // Values range from 0-1
}
```

EXAMPLES
--------

```c++
/* Example Arduino code using the HSVtoRGB function
 * Written by UseAfterFree
 * https://www.gitlab.com/UseAfterFreee/hsvtorgb
 */

#include "HSVtoRGB.hpp"

const int red_pin = 9;
const int green_pin = 10;
const int blue_pin = 11;

int hue = 0;

void WriteColor(RGB col)
{
    // Writes the color to the pins note that we are multiplying the color values with 255
    // because HSVtoRGB returns the values in a range of 0-1
    analogWrite(red_pin,   col.r*255);
    analogWrite(green_pin, col.g*255);
    analogWrite(blue_pin,  col.b*255);
}

void setup()
{
    pinMode(red_pin,   OUTPUT);
    pinMode(green_pin, OUTPUT);
    pinMode(blue_pin,  OUTPUT);
}

void loop()
{
    if (hue < 0) hue = 0; // Overflow protection
    RGB color = HSVtoRGB(hue++, 1, 1); // Full saturation and value, incrementing hue each time 
    WriteColor(color);

    delay(10);
}
```

FILES
-----

This library uses two files HSVtoRGB.hpp and HSVtoRGB.cpp

 * HSVtoRGB.hpp - Header file containing declarations this file should be included if you want to use the functions or RGB struct
 * HSVtoRGB.cpp - Source file containing function definitions, do not include this file directly but do make sure to compile it or link a precompiled version

NOTES
-----

Using a negative hue value will result in undefined behaviour.
If saturation or value is out of the bounds ([0-1]) a RGB value of 0,0,0 will be returned;
