#ifndef HSVTORGB_HPP
#define HSVTORGB_HPP

struct RGB
{
    float r, g, b;
};

RGB& operator+=(RGB& col, float val);

RGB HSVtoRGB(float hue, float saturation, float value);

#endif
